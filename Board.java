public class Board {
	
	private Die d1;
	private Die d2;
	private boolean[] closedTiles;
	
	public Board() {
		this.d1 = new Die();
		this.d2 = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString() {
		String s = "";
		int count = 0;
		for (boolean tile : this.closedTiles) {
			if(this.closedTiles[count])
				s = s + "X ";
			else
				s = s + (count + 1) + " ";
			count++;
		}
		return s;
	}
	
	public boolean playATurn() {
		this.d1.roll();
		this.d2.roll();
		System.out.println(this.d1 + " " + this.d2);
		
		int totalPips = this.d1.getPips() + this.d2.getPips();
		if(this.closedTiles[totalPips - 1]){
			System.out.println("The tile at this position is already shut.");
			return true;
		}
		else {
			this.closedTiles[totalPips - 1] = true;
			System.out.println("Closing tile: " + totalPips);
			return false;
		}
		
	}
	
}