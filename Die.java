import java.util.Random;
public class Die {
	
	private int pips;
	private Random r;
	
	public Die() {
		this.pips = 1;
		this.r = new Random();
	}
	
	public int getPips() {
		return this.pips;
	}
	
	public int roll() {
		this.pips = r.nextInt(1, 7);
		return this.pips;
	}
	
	public String toString() {
		return "" + this.pips;
	}
	
}