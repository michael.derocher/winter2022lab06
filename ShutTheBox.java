public class ShutTheBox {
	
	public static void main(String[] args) {
		
		System.out.println("Welcome to Shut The Box. Let the game Begin!");
		boolean gameOver = false;
		
		java.util.Scanner reader = new java.util.Scanner(System.in);
		int p1Wins = 0;
		int p2Wins = 0;
		boolean keepPlaying = true;
		char ans;
		while(keepPlaying){
		
			Board b = new Board();
			gameOver = false;
		
			while(!gameOver){
			
				System.out.println("Player 1's turn");
				System.out.println(b);
			
				if(b.playATurn()) {
					System.out.println("Player 2 wins!");
					p2Wins++;
					gameOver = true;
				}
				else {
					System.out.println("Player 2's turn");
					System.out.println(b);
				
					if(b.playATurn()) {
						System.out.println("Player 1 wins!");
						p1Wins++;
						gameOver = true;
					}
					
				}
			
			}
			System.out.println("Play again? (y / n)");
			ans = reader.next().charAt(0);
			if(ans == 'n')
				keepPlaying = false;
			else if(ans == 'y')
				System.out.println("Starting new game:");
			else {
				System.out.println("Invalid response. Ending program.");
				keepPlaying = false;
			}
				
			
		}
		System.out.println("Final score: " + p1Wins + " - " + p2Wins);
	}
	
}